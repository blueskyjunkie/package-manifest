#
# Copyright 2018 Russell Smiley
#
# This file is part of package-manifest.
#
# package-manifest is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# package-manifest is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with package-manifest.  If not, see <http://www.gnu.org/licenses/>.
#

import fnmatch
import os

from ..path import \
    filterNotInDirectory

from ..path.path import PathOperation

from ..yamlParser import \
    Directory, \
    Files, \
    YamlNotFound


def applyRecursiveInclusions( pathNames, directory, inclusions ) :
    """
    Apply an iterable of unix glob wildcard inclusion patterns recursively to an iterable of pathnames from the
    specified directory.

    Assume that path names, directory and inclusions are expressed relative to the same root directory.
    
    :param pathNames: path names to be filtered expressed relative to the root directory of the package-manifest.
    :param directory: directory to apply patterns to expressed relative to the root directory of the package-manifest.
    :param inclusions: include patterns

    :return: set of path names after inclusions have been applied.
    """

    currentDirNames = filterNotInDirectory( pathNames, directory )

    aggregateIncludes = set()
    for thisInclude in inclusions :
        namesMatchInclusion = { x for x in currentDirNames if fnmatch.fnmatch( os.path.basename( x ), thisInclude ) }
        aggregateIncludes |= namesMatchInclusion

    return aggregateIncludes


class Include( PathOperation ) :
    __yamlKey = 'recursive-include'


    def __init__( self, directory, includePattern ) :
        self.directory = directory
        self.includePattern = includePattern


    def apply( self, originalPathNames, currentPathNames ) :
        assert all( [ x in originalPathNames for x in currentPathNames ] )

        includedNames = applyRecursiveInclusions( originalPathNames, self.directory, [ self.includePattern ] )
        modifiedNames = currentPathNames | includedNames

        return modifiedNames


    @classmethod
    def from_yamlData( cls, yamlData ) :
        if yamlData is None :
            raise YamlNotFound( 'yamlData is None' )
        elif Include.__yamlKey not in yamlData :
            raise YamlNotFound( '{1} not found in yaml data, {0}'.format( yamlData, Include.__yamlKey ) )
        else :
            try :
                directory = Directory.from_yamlData( yamlData[ Include.__yamlKey ] )
                filePatterns = Files.from_yamlData( yamlData[ Include.__yamlKey ] )

                result = list()
                for thisPattern in filePatterns.patterns :
                    result.append( cls( directory.path, thisPattern ) )
            except YamlNotFound as e :
                raise YamlNotFound(
                    '{1} must use files and directory directives, {0}'.format( yamlData, Include.__yamlKey ) ) from e

        return result
